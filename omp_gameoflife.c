#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <sys/time.h>

#include <omp.h>

#define calcIndex(width, x,y)  ((y)*(width) + (x))

#define GHOSTLAYERPADDING 2

struct calcField{
   uint xs;
   uint xe;
   uint ys;
   uint ye;
};

struct calcField calcFieldSize(int threadID, int w, int h, int px, int py)
{
   struct calcField field = { 0 };
   int ww, wh;

   //calculate generic working width and height for each thread
   ww = (w - GHOSTLAYERPADDING) / (px + 1);
   wh = (h - GHOSTLAYERPADDING) / (py + 1);

   //calculate start x y for each individual thread
   field.xs = 1 + ww * (threadID % (px + 1));
   field.ys = 1 + wh * (threadID / (px + 1));

   //calculate end x y for each individual thread
   field.ye = field.ys + wh - 1;
   field.xe = field.xs + ww - 1;

   //overwrite x end if there is overlapping
   if((threadID % (px + 1) == px)) {
      field.xe = (w - GHOSTLAYERPADDING);
   }

   //overwrite y end if there is overlapping
   if((threadID / (px + 1) == py)) {
      field.ye = (h - GHOSTLAYERPADDING);
   }

   return field;
}

void writeVTK2Piece(long timestep, double *data, char prefix[1024], int w, int h, struct calcField field, int threadID) {
   char filename[2048];
   int x,y;

   float deltax=1.0;
   long  nxy = (field.xe - field.xs + 1 + 1) * (field.ye - field.ys + 1 + 1) * sizeof(float);

   snprintf(filename, sizeof(filename), "%s-t%03i-%05ld.vti", prefix, threadID, timestep);
   FILE* fp = fopen(filename, "w");

   fprintf(fp, "<?xml version=\"1.0\"?>\n");
   fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
   fprintf(fp, "\t<ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n",  (field.xs - 1), (field.xe), (field.ys - 1), (field.ye), 0, 0, deltax, deltax, 0.0);
   fprintf(fp, "\t\t<CellData Scalars=\"%s\">\n", prefix);
   fprintf(fp, "\t\t\t<DataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
   fprintf(fp, "\t\t</CellData>\n");
   fprintf(fp, "\t</ImageData>\n");
   fprintf(fp, "<AppendedData encoding=\"raw\">\n");
   fprintf(fp, "_");
   fwrite((unsigned char*)&nxy, sizeof(long), 1, fp);

   for (y = (field.ys); y <= (field.ye); y++) {
      for (x = (field.xs); x <= (field.xe); x++) {
         float value = data[calcIndex(w, x, y)];
         fwrite((unsigned char*)&value, sizeof(float), 1, fp);
      }
   }

   fprintf(fp, "\n</AppendedData>\n");
   fprintf(fp, "</VTKFile>\n");
   fclose(fp);
}

void writeVTKMaster(long timestep, char prefix[1024], int w, int h, int px, int py, int totalThreadCount) {
   char filename[2048], piecename[2048];
   struct calcField field;

   snprintf(filename, sizeof(filename), "%s-%05ld%s", prefix, timestep, ".pvti");
   FILE* fp = fopen(filename, "w");

   fprintf(fp, "<?xml version=\"1.0\"?>\n");
   fprintf(fp, "<VTKFile type=\"PImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
   fprintf(fp, "\t<PImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", 0, (w - GHOSTLAYERPADDING), 0 , (h - GHOSTLAYERPADDING), 0, 0, 1.0, 1.0, 0.0);
   fprintf(fp, "\t\t<PCellData Scalars=\"%s\">\n", prefix);
   fprintf(fp, "\t\t\t<PDataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
   fprintf(fp, "\t\t</PCellData>\n");
   for(int threadCounter = 0; threadCounter < totalThreadCount; threadCounter++)
   {
      field = calcFieldSize(threadCounter, w, h, px, py);

      snprintf(piecename, sizeof(piecename), "%s-t%03i-%05ld.vti", prefix, threadCounter, timestep);
      fprintf(fp, "\t\t<Piece Extent=\"%i %i %i %i 0 0\" Source=\"%s\"/>\n", (field.xs - 1), (field.xe), (field.ys - 1), (field.ye), piecename);
   }

   fprintf(fp, "\t</PImageData>\n");
   fprintf(fp, "</VTKFile>\n");
   fclose(fp);
}


void show(double* currentfield, int w, int h) {
   printf("\033[H");
   int x,y;
   for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) printf(currentfield[calcIndex(w, x,y)] ? "\033[07m  \033[m" : "  ");
      //printf("\033[E");
      printf("\n");
   }
   fflush(stdout);
}


void evolve(double* currentfield, double* newfield, int w, int h, int px, int py, int t, bool vti) {
   int x,y;
   int id = omp_get_thread_num();

   struct calcField field;
   field = calcFieldSize(id, w, h, px, py);

   for (y = field.ys; y <= field.ye; y++) {
      for (x = field.xs; x <= field.xe; x++) {
         double newState = 0;
         int neighborCount = 0;

         neighborCount += currentfield[calcIndex(w, x - 1, y - 1)];
         neighborCount += currentfield[calcIndex(w, x,     y - 1)];
         neighborCount += currentfield[calcIndex(w, x + 1, y - 1)];

         neighborCount += currentfield[calcIndex(w, x - 1, y    )];
         neighborCount += currentfield[calcIndex(w, x + 1, y    )];

         neighborCount += currentfield[calcIndex(w, x - 1, y + 1)];
         neighborCount += currentfield[calcIndex(w, x,     y + 1)];
         neighborCount += currentfield[calcIndex(w, x + 1, y + 1)];

         if(neighborCount == 2) {
         newState = currentfield[calcIndex(w, x, y)];
         } else if(neighborCount == 3) {
         newState = 1;
         } else {
         newState = 0;
         }

         newfield[calcIndex(w, x,y)] = newState;
      }
   }

   if(vti) {
      writeVTK2Piece(t, currentfield, "gol", w, h, field, id);
   }
}

void randomFilling(double* currentfield, int w, int h) {
   int x,y;

   for (y = 1; y < (h - 1); y++) {
      for (x = 1; x < (w - 1); x++) {
         currentfield[calcIndex(w, x, y)] = (rand() < RAND_MAX / 10) ? 1 : 0;
      }
   }
}

void pathFilling(double* currentfield, int w, int h, char *path) {
   FILE *fp;
   int bufferSize = 100, total;
   char buffer[bufferSize];

   fp = fopen(path, "r");
   if(fp == NULL) {
      printf("File couldn't be opened!\n");
      exit(EXIT_FAILURE);
   }

   total = 0;

   while(fread(buffer, sizeof(char), bufferSize, fp) != EOF)
   {
      for(int i = 0; i < bufferSize && total < (w * h); i++) {
         for(int j = 0; j < 8; j++) {
            currentfield[total] = (buffer[i] >> j) & 0x01;
            total++;

            if(total >= (w * h))
               { break; }
         }

         if(total >= (w * h))
            { break; }
      }

      if(total >= (w * h))
         { break; }
   }

   fclose(fp);
}

void game(int z, int w, int h, int px, int py, char *path, bool gfx, bool vti) {
   w += GHOSTLAYERPADDING;
   h += GHOSTLAYERPADDING;
   double *currentfield = calloc(w * h, sizeof(double));
   double *newfield     = calloc(w * h, sizeof(double));

   int threads = (px + 1) * (py + 1);

   if(path[0] != '\0') {
      pathFilling(currentfield, w, h, path);
   } else {
      randomFilling(currentfield, w, h);
   }

   long t;

   for (t = 0; t < z; t++) {

      if(gfx) {
         show(currentfield, w, h);
      }

      if(vti) {
         writeVTKMaster(t, "gol", w, h, px, py, threads);
      }


      #pragma omp parallel num_threads(threads) shared(currentfield, newfield)
      {
         evolve(currentfield, newfield, w, h, px, py, t, vti);
      }

      //printf("\r%ld timestep", (t + 1));

      //SWAP
      double *temp = currentfield;
      currentfield = newfield;
      newfield = temp;
   }

   //printf("\n");

   free(currentfield);
   free(newfield);

}

static struct option long_options[] = {
   {  "width",       optional_argument,   NULL, 'w'},
   {  "height",      optional_argument,   NULL, 'h'},
   {  "xcuts",       optional_argument,   NULL, 'x'},
   {  "ycuts",       optional_argument,   NULL, 'y'},
   {  "iterations",  optional_argument,   NULL, 'i'},
   {  "path",        optional_argument,   NULL, 'p'},
   {  "disablegfx",  no_argument,         NULL, 'g'},
   {  "disablevti",  no_argument,         NULL, 'v'},
   {  NULL,          no_argument,         NULL, 0 }
};

int main(int argc, char **argv) {
   int w = 30, h = 30, px = 1, py = 1, i = 100, pathLength=256;
   char *path;
   bool gfx = true, vti = true;

   path = calloc(pathLength, sizeof(char));
   if(path == NULL)
      { printf("ENOMEM\n"); }

   int c, option_index = 0;
   while((c = getopt_long(argc, argv, "w:h:x:y:i:p:gv", long_options, &option_index)) != -1) {

      switch (c)
      {
         case 'w': w = atoi(optarg); break;
         case 'h': h = atoi(optarg); break;
         case 'x': px = atoi(optarg); break;
         case 'y': py = atoi(optarg); break;
         case 'i': i = atoi(optarg); break;
         case 'p': strncpy(path, optarg, pathLength); break;
         case 'g': gfx = false; break;
         case 'v': vti = false; break;
         default: exit(EXIT_FAILURE); break;
      }
  }

  game(i, w, h, px, py, path, gfx, vti);
  free(path);
}
