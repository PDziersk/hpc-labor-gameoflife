# the compiler: gcc for C program, define as g++ for C++
CC = gcc

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g -Wall -std=c99 -lc -D _BSD_SOURCE -fopenmp -lm -o2


# the build target executable:
TARGET = gameoflife

all: mpi omp

omp: omp_$(TARGET).c
	$(CC) $(CFLAGS) -o omp_$(TARGET) omp_$(TARGET).c

mpi: mpi_gameoflife.c
	mpicc -o mpi_$(TARGET) mpi_$(TARGET).c

clean: cleantmp
	$(RM) omp_$(TARGET)
	$(RM) mpi_$(TARGET)

cleantmp:
	find . -name "*.vti" -print0 | xargs -0 $(RM)
	$(RM) *.pvti
