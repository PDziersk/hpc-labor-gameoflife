#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>

#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <sys/time.h>

#include "mpi.h"

#define calcIndex(width, x,y)  ((y)*(width) + (x))

#define GHOSTLAYERPADDING 2

#define PATHLENGTH 256

#define MAX_CHILD_COUNT 10

struct calcField{
   uint xs;
   uint xe;
   uint ys;
   uint ye;
};



struct calcField calcFieldSize(int threadID, int w, int h, int px, int py)
{
   struct calcField field = { 0 };
   int ww, wh;

   //calculate generic working width and height for each thread
   ww = (w - GHOSTLAYERPADDING) / (px + 1);
   wh = (h - GHOSTLAYERPADDING) / (py + 1);

   //calculate start x y for each individual thread
   field.xs = 1 + ww * (threadID % (px + 1));
   field.ys = 1 + wh * (threadID / (px + 1));

   //calculate end x y for each individual thread
   field.ye = field.ys + wh - 1;
   field.xe = field.xs + ww - 1;

   //overwrite x end if there is overlapping
   if((threadID % (px + 1) == px)) {
      field.xe = (w - GHOSTLAYERPADDING);
   }

   //overwrite y end if there is overlapping
   if((threadID / (px + 1) == py)) {
      field.ye = (h - GHOSTLAYERPADDING);
   }

   return field;
}

#define GHOSTLAYER 1
#define INDEXALIGN 1

void writeVTK2Piece(long timestep, double *data, char prefix[1024], struct calcField *field, int threadID) {
   char filename[2048];
   int x,y;

   int fieldWidth = (field -> xe - field -> xs + INDEXALIGN + GHOSTLAYERPADDING);
   int fieldHeight = (field -> ye - field -> ys + INDEXALIGN + GHOSTLAYERPADDING);

   float deltax = 1.0;
   long  nxy = (field -> xe - field -> xs + INDEXALIGN) * (field -> ye - field -> ys + INDEXALIGN) * sizeof(float);

   snprintf(filename, sizeof(filename), "%s-t%03i-%05ld.vti", prefix, threadID, timestep);
   FILE* fp = fopen(filename, "w");

   fprintf(fp, "<?xml version=\"1.0\"?>\n");
   fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
   fprintf(fp, "\t<ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n",  (field -> xs - INDEXALIGN), (field -> xe), (field -> ys - INDEXALIGN), (field -> ye), 0, 0, deltax, deltax, 0.0);
   fprintf(fp, "\t\t<CellData Scalars=\"%s\">\n", prefix);
   fprintf(fp, "\t\t\t<DataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
   fprintf(fp, "\t\t</CellData>\n");
   fprintf(fp, "\t</ImageData>\n");
   fprintf(fp, "<AppendedData encoding=\"raw\">\n");
   fprintf(fp, "_");
   fwrite((unsigned char*)&nxy, sizeof(long), 1, fp);

   for (y = 1; y < (fieldHeight - GHOSTLAYER); y++) {
      for (x = 1; x < (fieldWidth - GHOSTLAYER); x++) {
         float value = data[calcIndex(fieldWidth, x, y)];
         fwrite((unsigned char*)&value, sizeof(float), 1, fp);
      }
   }

   fprintf(fp, "\n</AppendedData>\n");
   fprintf(fp, "</VTKFile>\n");
   fclose(fp);
}



void writeVTKMaster(long timestep, char prefix[1024], int w, int h, int px, int py, int totalThreadCount) {
   char filename[2048], piecename[2048];
   struct calcField field;

   snprintf(filename, sizeof(filename), "%s-%05ld%s", prefix, timestep, ".pvti");
   FILE* fp = fopen(filename, "w");

   fprintf(fp, "<?xml version=\"1.0\"?>\n");
   fprintf(fp, "<VTKFile type=\"PImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
   fprintf(fp, "\t<PImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", 0, (w - GHOSTLAYERPADDING), 0 , (h - GHOSTLAYERPADDING), 0, 0, 1.0, 1.0, 0.0);
   fprintf(fp, "\t\t<PCellData Scalars=\"%s\">\n", prefix);
   fprintf(fp, "\t\t\t<PDataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
   fprintf(fp, "\t\t</PCellData>\n");
   for(int threadCounter = 0; threadCounter < totalThreadCount; threadCounter++)
   {
      field = calcFieldSize(threadCounter, w, h, px, py);

      snprintf(piecename, sizeof(piecename), "%s-t%03i-%05ld.vti", prefix, threadCounter, timestep);
      fprintf(fp, "\t\t<Piece Extent=\"%i %i %i %i 0 0\" Source=\"%s\"/>\n", (field.xs - 1), (field.xe), (field.ys - 1), (field.ye), piecename);
   }

   fprintf(fp, "\t</PImageData>\n");
   fprintf(fp, "</VTKFile>\n");
   fclose(fp);
}



bool evolve(double* currentfield, double* newfield, int w, int h) {
   int x,y;
   bool hasEvolved;

   hasEvolved = false;

   for (y = 1; y <= (h - 1); y++) {
      for (x = 1; x <= (w - 1); x++) {
         double newState = 0;
         int neighborCount = 0;

         neighborCount += currentfield[calcIndex(w, x - 1, y - 1)];
         neighborCount += currentfield[calcIndex(w, x,     y - 1)];
         neighborCount += currentfield[calcIndex(w, x + 1, y - 1)];

         neighborCount += currentfield[calcIndex(w, x - 1, y    )];
         neighborCount += currentfield[calcIndex(w, x + 1, y    )];

         neighborCount += currentfield[calcIndex(w, x - 1, y + 1)];
         neighborCount += currentfield[calcIndex(w, x,     y + 1)];
         neighborCount += currentfield[calcIndex(w, x + 1, y + 1)];

         if(neighborCount == 2) {
            newState = currentfield[calcIndex(w, x, y)];
         } else if(neighborCount == 3) {
            if(currentfield[calcIndex(w, x, y)] == 0) {
               hasEvolved = true;
            }

            newState = 1;
         } else {
            if(currentfield[calcIndex(w, x, y)] == 1) {
               hasEvolved = true;
            }
            newState = 0;
         }



         newfield[calcIndex(w, x,y)] = newState;
      }
   }

   return hasEvolved;
}



static struct option long_options[] = {
   {  "width",       optional_argument,   NULL, 'w'},
   {  "height",      optional_argument,   NULL, 'h'},
   {  "xcuts",       optional_argument,   NULL, 'x'},
   {  "ycuts",       optional_argument,   NULL, 'y'},
   {  "iterations",  optional_argument,   NULL, 'i'},
   {  "path",        optional_argument,   NULL, 'p'},
   {  "disablegfx",  no_argument,         NULL, 'g'},
   {  "disablevti",  no_argument,         NULL, 'v'},
   {  NULL,          no_argument,         NULL, 0 }
};



struct game_options {
   int width;
   int height;
   int px;
   int py;
   int iterations;
   char *path;
   bool vti;
   bool gfx;
};



void randomFilling(double* currentfield, int w, int h, int rank) {
   int x,y;
   int seed = time(NULL);
   seed = (seed << rank) & __UINT32_MAX__;
   srand(seed);
   for (y = 1; y < (h - 2); y++) {
      for (x = 1; x < (w - 2); x++) {
         currentfield[calcIndex(w, x, y)] = (rand() < RAND_MAX / 10) ? 1 : 0;
      }
   }
}



void parseParameter(int argc, char **argv, struct game_options *options)
{
   int c, option_index = 0;
   while((c = getopt_long(argc, argv, "w:h:x:y:i:p:gv", long_options, &option_index)) != -1) {

      switch (c)
      {
         case 'w': options -> width = atoi(optarg); break;
         case 'h': options -> height = atoi(optarg); break;
         case 'x': break; //options -> px = atoi(optarg); break;
         case 'y': break; //options -> py = atoi(optarg); break;
         case 'i': options -> iterations = atoi(optarg); break;
         case 'p': strncpy(options -> path, optarg, PATHLENGTH); break;
         case 'g': options -> gfx = false; break;
         case 'v': options -> vti = false; break;
         default: exit(EXIT_FAILURE); break;
      }
   }
}

bool hasRightNeighbor(int x, int width) {
   return x != (width - INDEXALIGN - GHOSTLAYER);
}

bool hasLeftNeighbor(int x) {
   return x != 1;
}

int main(int argc, char **argv) {
   int pathLength=256;
   struct game_options options = {0};
   int evolved, evolvedAreas;
   bool hasEvolved;


   options.path = calloc(PATHLENGTH, sizeof(char));

   if(options.path == NULL) {
      printf("ENOMEM\n");
      exit(EXIT_FAILURE);
   }

   options.width = 30;
   options.height = 30;
   options.px = 0;
   options.py = 0;
   options.iterations = 100;
   options.vti = true;
   options.gfx = true;

   parseParameter(argc, argv, &options);

   options.width += GHOSTLAYERPADDING;
   options.height += GHOSTLAYERPADDING;



   int myrank, size;
   MPI_Status status;
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
   MPI_Comm_size(MPI_COMM_WORLD, &size);

   options.px = size - 1;

   int i;

   struct calcField field;

   field = calcFieldSize(myrank, options.width, options.height, options.px, options.py);
   //printf("xs: %i xe: %i ys: %i ye: %i\n", field.xs, field.xe, field.ys, field.ye);

   double *currentField;
   double *evolvedField;

   int partialWidth = field.xe - field.xs + 1 + GHOSTLAYERPADDING;
   int partialHeight = field.ye - field.ys + 1 + GHOSTLAYERPADDING;

   currentField = calloc(partialHeight * partialWidth, sizeof(double));
   evolvedField = calloc(partialHeight * partialWidth, sizeof(double));

   if(currentField == NULL || evolvedField == NULL) {
      exit(-ENOMEM);
   }

   randomFilling(currentField, partialWidth, partialHeight, myrank);

   for(int iteration = 0; iteration < options.iterations; iteration++) {
      //Randaustausch
      MPI_Request leftRecvRequest = MPI_REQUEST_NULL,
                  leftSendRequest = MPI_REQUEST_NULL,
                  rightRecvRequest = MPI_REQUEST_NULL,
                  rightSendRequest = MPI_REQUEST_NULL;

      MPI_Status  leftRecvStatus = { 0 },
                  leftSendStatus = { 0 },
                  rightRecvStatus = { 0 },
                  rightSendStatus = { 0 };

      double *rightNeighborData = NULL,
             *rightData = NULL,
             *leftNeighborData = NULL,
             *leftData = NULL;

      //Start right Jobs Async
      if(hasRightNeighbor(field.xe, options.width)) {
         int rightNeighborId = myrank + 1;
         //printf("%i: I have a right neighbor - id: %i\n", myrank, rightNeighborId);

         //TODO: calloc outside iteration loop
         rightNeighborData = calloc(partialHeight, sizeof(double));
         rightData = calloc(partialHeight, sizeof(double));

         if(rightData == NULL || rightNeighborData == NULL) {
            exit(-ENOMEM);
         }

         //TODO: Pointer erläutern
         for(int line = 0; line < partialHeight; line++) {
            int myDataPoint = line * partialWidth + partialWidth - 1;
            //printf("%i: myRDataPoint: %i\n", myrank, myDataPoint);
            rightData[line] = currentField[myDataPoint];
         }

         MPI_Isend(rightData, partialHeight, MPI_DOUBLE, rightNeighborId, 1337, MPI_COMM_WORLD, &rightSendRequest);
         MPI_Irecv(rightNeighborData, partialHeight, MPI_DOUBLE, rightNeighborId, 1337, MPI_COMM_WORLD, &rightRecvRequest);
      }

      //Start left Jobs Async
      if(hasLeftNeighbor(field.xs)) {
         int leftNeighborId = myrank - 1;
         //printf("%i: I have a left neighbor - id: %i\n", myrank, leftNeighborId);

         leftNeighborData = calloc(partialHeight, sizeof(double));
         leftData = calloc(partialHeight, sizeof(double));

         if(leftData == NULL || leftNeighborData == NULL) {
            exit(-ENOMEM);
         }

         for(int line = 0; line < partialHeight; line++) {
            int myDataPoint = line * partialWidth + 1;
            //printf("%i: myLDataPoint: %i\n", myrank, myDataPoint);
            leftData[line] = currentField[myDataPoint];
         }

         MPI_Isend(leftData, partialHeight, MPI_DOUBLE, leftNeighborId, 1337, MPI_COMM_WORLD, &leftSendRequest);
         MPI_Irecv(leftNeighborData, partialHeight, MPI_DOUBLE, leftNeighborId, 1337, MPI_COMM_WORLD, &leftRecvRequest);
      }

      //Wait and Work on right recv Job
      if(hasRightNeighbor(field.xe, options.width)) {
         MPI_Wait(&rightRecvRequest, &rightRecvStatus);

         if(rightRecvStatus.MPI_ERROR != 0) {
            printf("%i: error recv right ghostlayer %i", myrank, rightRecvStatus.MPI_ERROR);
            exit(-EIO);
         }

         for(int line = 0; line < partialHeight; line++) {
            int myDataPoint = line * partialWidth + partialWidth - 1;
            //printf("%i: myRGhostlayerPoint: %i\n", myrank, myDataPoint);
            currentField[myDataPoint] = rightNeighborData[line];
         }

         free(rightNeighborData);
      }

      //Wait and Work on left recv Job
      if(hasLeftNeighbor(field.xs)) {
         MPI_Wait(&leftRecvRequest, &leftRecvStatus);

         if(leftRecvStatus.MPI_ERROR != 0) {
            printf("%i: error recv right ghostlayer %i", myrank, leftRecvStatus.MPI_ERROR);
            exit(-EIO);
         }

         for(int line = 0; line < partialHeight; line++) {
            int myDataPoint = line * partialWidth;
            //printf("%i: myLGhostlayerPoint: %i\n", myrank, myDataPoint);
            currentField[myDataPoint] = leftNeighborData[line];
         }

         free(leftNeighborData);
      }

      hasEvolved = evolve(currentField, evolvedField, partialWidth, partialHeight);

      if(myrank == 0 && options.vti) {
         writeVTKMaster(iteration, "gol", options.width, options.height, options.px, options.py, size);
      }

      if(options.vti) {
         writeVTK2Piece(iteration, currentField, "gol", &field, myrank);
      }

      double *tmp = evolvedField;
      evolvedField = currentField;
      currentField = tmp;

      MPI_Wait(&rightSendRequest, &rightSendStatus);
      free(rightData);
      if(rightSendStatus.MPI_ERROR != 0) {
         printf("%i: error recv right ghostlayer %i", myrank, rightSendStatus.MPI_ERROR);
         exit(-EIO);
      }

      MPI_Wait(&leftSendRequest, &leftSendStatus);
      free(leftData);
      if(leftSendStatus.MPI_ERROR != 0) {
         printf("%i: error recv left ghostlayer %i", myrank, leftSendStatus.MPI_ERROR);
         exit(-EIO);
      }

      if(hasEvolved) {
         evolved = 1;
      } else {
         evolved = 0;
      }

      MPI_Allreduce(&evolved, &evolvedAreas, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

      if(evolvedAreas == 0) {
         printf("%i: evolved: %i evolvedAreas: %i - %i\n", myrank, evolved, evolvedAreas, iteration);
         break;
      }
   }

   MPI_Finalize();
   free(options.path);
   free(currentField);
   free(evolvedField);
}
